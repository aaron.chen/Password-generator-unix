# Password-generator-unix
Password Generator for Unix using Ruby. Uses mix of magic constants, time, mersenne twister and Linux's own secure randomness to generate passwords. Unix version of [this.](https://github.com/ohgeedubs/Password-generator-windows)

To run, simply run `ruby ./passgen`
This will generate a password that is displayed and copied to the clipboard. After this, you can type in commands to alter options for other passwords you want to generate.

* "h" or "help" - shows all the commands
* "symoff" - disable use of symbols in password.
* "symon" - enable use of symbols in password.
* "[n]" - typing in an integer will give you a password of n length
* "end" or "exit" - exit the program
